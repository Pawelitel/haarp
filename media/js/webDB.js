//модуль опеспечивающий работу с HTML5 Web SQL Database
//Максимальный рекомендуемый для браузеров размер базы данных 5Мб.
//При запросе большей базы данных, браузер должен выдавать сообщение пользователю с предупреждением об опасности и просьбой подтвердить большой размер базы данных.

//менеджер базы данных
var dbManagerParser = (function (dbName, dbVersion)
{
    var db = null,
        dbDiscription = "Hockey Arena Analytical Research Plugin Offline Database",
        dbEstimatedSize = 100 * 1024 * 1024,
		Qimgs = 'http://www.hockeyarena.net/pics/target.gif'
		;

    //инициализация базы
    function create_tables(dbManagerDB) 
	{
		db=dbManagerDB;
        console.log('Подготовка базы');
        try {
            if (window.openDatabase) {
                //db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
                if (db) {
                    db.transaction(function (tx,e)
					{
                        var queryArray = [];
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_public_player_info (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, country TEXT,contract integer,team_id integer,injury integer,performance integer,performanceQuestion integer,potential integer,potentialQuestion integer,ability_index integer,satisfaction integer,weeks_in_team integer,add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_training_form1 (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, goa integer, def integer, att integer, sho integer, pas integer, spe integer, str integer, sc integer, ene integer, form integer, tra integer, schedule text, pos text ,add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_team_players (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, perf integer, perfq integer, pot integer, potq integer, skill integer, ai integer, wit integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_team_contracts (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, contract integer, salary integer, perf integer, perfq integer, pot integer, potq integer, loy integer, countryShort text, ai integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_player_market_sql (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, goa integer, def integer, att integer, sho integer, pas integer, spe integer, str integer, sc integer, ai integer, skill integer, perf integer, perfq integer, pot integer, potq integer, contract text, nat text, price integer, deadline DATETIME, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_transfer_list_sql (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, day TEXT, price integer, team_from_name TEXT, team_from_id integer, man_from_name TEXT, man_from_id integer, team_to_name TEXT, team_to_id integer, man_to_name TEXT, man_to_id integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_daily (record_id integer PRIMARY KEY AUTOINCREMENT, page_name TEXT, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_public_league_standings (record_id integer PRIMARY KEY AUTOINCREMENT, league_name TEXT, position integer, team_id integer, team_name TEXT, mat integer, win integer, drw integer, lose integer, wo integer, lo integer, pts integer, g_p integer, g_n integer, pn integer, home TEXT, away TEXT, add_date_time DATETIME);");
                        for (var i = 0; i < queryArray.length; i++) {
                            //notice(queryArray[i]);
                            if ((queryArray[i]) != undefined) tx.executeSql(queryArray[i], [], function (tx, result) {
                                //notice('inserted');
                            }, function (tx,e) {
                                error_notice('Ошибка при создании таблицы: код ' +e.code + ": " + e.message);
                            });
                            //notice(queryArray[i]);
                        }
						window.queryArray = null;
                    });
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }

	function getUrlVars(href)
	{
        var vars = [],
            hash;
		//if(href==undefined) return undefined;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        if (vars["p"] == undefined) {
            vars = [];
            var pageName = href.slice(href.lastIndexOf('/') + 1);
            vars.push(pageName);
            vars["p"] = pageName;
        }
        return vars;
    }
	
	//возвращает имя файла из пути/имени js
	function hname(href)
	{
		return href.slice(href.lastIndexOf('/') + 1);
	}
	
	function notice(notice_text, time)
	{
 		console.log(notice_text);
		if(undefined==time)
			time=5000;
		noty({
			"text": notice_text,
			"theme": "noty_theme_twitter",
			"layout": "bottomLeft",
			"type": "information",
			"animateOpen": {"height":"toggle","opacity":"show"},
			"animateClose": {"height":"toggle","opacity":"hide"},
			"speed": 1000,
			"timeout": time,
			"closeButton": true,
			"closeOnSelfClick": true,
			"closeOnSelfOver": true,
			"modal": false
		});
	}
	
	function error_notice(notice_text)
	{
		console.log(notice_text);
		
		noty({
			"text": notice_text,
			"theme": "noty_theme_twitter",
			"layout": "bottomLeft",
			"type": "error",
			"animateOpen": {"height":"toggle","opacity":"show"},
			"animateClose": {"height":"toggle","opacity":"hide"},
			"speed": 300,
			"timeout": false,
			"closeButton": true,
			"closeOnSelfClick": true,
			"closeOnSelfOver": false,
			"modal": false
		});
	}

    //функция получает #page со страницы игрока
    //и из него получает нужные данные для вставки в таблицу
    function collect_public_player_info(page)
	{
		if($.trim($(page).find("table tbody tr").text())!="Игрок отсутствует или закончил карьеру !")
		if($.trim($(page).find("table tbody tr").text())!="Wrong player_id")
		{
			var trs = $(page).find("table tbody tr");
			var player_id = $.trim($(trs[0]).text()).split(",")[1].substr(4);
			var name = ($.trim($(trs[0]).text()).split(",")[0].split("игроке ")[1]);
			var age = $($(trs[1]).find(".q")[0]).text();
			var country = $.trim($($(trs[1]).find(".q")[1]).text())
			var contract = $($(trs[2]).find(".q")[0]).text().split(" ")[0];

			var team_id = getUrlVars($(trs[2]).find("a").attr("href"))['team_id'];
			var injury = 0;
			if ($($(trs[3]).find(".q")[0]).text()!="Здоров")
				var injury = $($(trs[3]).find(".q")[0]).text().split('(')[1].split(' ')[0];

			var perf = $.trim($($(trs[3]).find(".q")[1]).text()).replace('%', '');
			var perfQ = 0;
			if($($(trs[3]).find(".q")[1]).find("img").attr("src")==Qimgs)
				perfQ=1;

			var pot = $.trim($($(trs[4]).find(".q")[1]).text()).replace('%', '')
			var potQ = 0;
			if($($(trs[4]).find(".q")[1]).find("img").attr("src")==Qimgs)
				potQ=1;

			var ai = $.trim($($(trs[5]).find(".q")[0]).text());

			var satis = $.trim($.trim($($(trs[6]).find(".q")[0]).text()).split("(")[1].split(")")[0].replace('=', ''));
			var wit = $.trim($($(trs[6]).find(".q")[1]).text());

			//проверка на допустимость значений
			if (player_id < 1)
				error_notice("ошибка player_id");
			
			var queryArray = [];
			queryArray.push(
			[player_id, //номер игрока
			name, //имя
			age, //возраст
			country, //страна
			contract, //остаток контракта
			team_id, //команда
			injury, //дней травмы
			perf, //работоспособность
			perfQ, //работоспособность приблизительна(0) или точна(1)
			pot, //потенциал
			potQ, //потенциал приблизителен(0) или точен(1)
			ai, //сумма умений
			satis, //удовлетворённость
			wit //Недель в команде
			]);
			var query = "INSERT INTO history_public_player_info VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));";
			var page_name_list =[];
			page_name_list.push("history_public_player_info_"+player_id);
			return [query,queryArray,page_name_list];
		}else return undefined;
	}
   
	//парсит страницу на массив имён и ссылок
	function parse_public_team_info_players(page)
	{
		//notice("parse_public_team_info_players");
		var team_id=getUrlVars($(page).find("div a").attr("href"))["team_id"]
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		var top_tr = $(page).find("table tbody tr[class=darkbg]");
		var team_name = ($.trim($(top_tr).text()).split(",")[0].split("игроках ")[1]);
		if((trs.length<20)||(trs.length>55))
		{
			error_notice("Найдено " + (trs.length - 1) + " игроков в команде "+team_name + " id="+team_id);
			return 0;
		}
		if(undefined==team_id)
		{
			error_notice("team_id == undefined in 192");
			return 0;
		}
		notice("Найдено " + (trs.length - 1) + " игроков в команде "+team_name + " id="+team_id);
		var page_name_list=[], url_list=[];
		for (var i = 0; i < trs.length; i++)
		{
			var tds = $(trs[i]).find("td");
			var undef = $(tds[0]).find("a")[0];
			if (undef!=undefined)
			{
				var player_id = getUrlVars(undef.href)["id"];
				if(undefined==player_id)
				{
					error_notice("player_id == undefined in 206");
					return 0;
				}
				
				//teamIdTmp = getUrlVars(window.location.href)["team_id"];
				var href = "http://www.hockeyarena.net/ru/index.php?p=public_player_info.inc&id="+player_id;
				page_name_list.push("history_public_player_info_"+player_id);
				url_list.push(href);
			}
		}
		//notice([page_name_list,url_list]);
		return [page_name_list,url_list];
    }
	
	function parse_public_league_standings(page)
	{
        //notice("parse_public_league_standings");
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		//notice("Найдено " + (trs.length - 4) + " команд");
		//if(false)
		var team_id_arr = [];
		var page_name_list=[], url_list=[];
		for (var i = 0; i < trs.length; i++)
		{
			var team_id = getUrlVars($(trs[i]).find("a")[0].href)["team_id"];
			if(undefined==team_id)
			{
				error_notice("team_id == undefined in 234");
				error_notice($(trs[i]).find("a")[0].href);
				return 0;
			}else team_id_arr.push(team_id);
			var href = "http://www.hockeyarena.net/ru/index.php?p=public_team_info_players.php&team_id="+team_id;
			url_list.push(href);
			page_name_list.push("history_public_team_info_players_"+team_id);
		}
		return [page_name_list,url_list];
	}
	
	function collect_public_league_standings(page)
	{
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		var team_id_arr = [];
		for (var i = 0; i < trs.length; i++)
		{
			var team_id = getUrlVars($(trs[i]).find("a")[0].href)["team_id"];
			if(undefined==team_id)
			{
				error_notice("team_id == undefined in 254");
				return 0;
			}else team_id_arr.push(team_id);
		}
		var league_name = $($(page).find("option[selected]")[0]).text().substr(0,2)+";"+$($(page).find("option[selected]")[1]).attr("value")+";"+$($(page).find("option[selected]")[2]).attr("value");
		notice("Найдено " + (trs.length) + " команд в лиге " + league_name);
		if(trs.length<16)
		{
			error_notice("Команд меньше 16-и");
			return 0;
		}
		var queryArray = [];
		var page_name_list = [];
		for (var i = 0; i < trs.length; i++) 
		{
			if ($(trs[i]).attr("class") != "caption") if ($(trs[i]).attr("class") != "thead") 
			{
			   var tds = $(trs[i]).find("td");
				var position = $(tds[0]).text().trim().split(".")[0];
				var team_id = getUrlVars($(tds[0]).children().attr("href"))["team_id"];
				var team_name = $(tds[0]).children().text();
				var mat = $(tds[1]).text();
				var win = $(tds[2]).text();
				var drw = $(tds[3]).text();
				var lose =$(tds[4]).text();
				var wo =  $(tds[5]).text();
				var lo =  $(tds[6]).text();
				var pts = $(tds[7]).text();
				var g_p = $(tds[8]).text().split('/')[0];
				var g_n = $(tds[8]).text().split('/')[1];
				var pn =  $(tds[9]).text();
				var home =$(tds[10]).text();
				var away =$(tds[11]).text().trim();
											tds = [];
				// 
				queryArray.push(
				[league_name, position, team_id, team_name, mat, win, drw, lose, wo, lo, pts, g_p, g_n, pn, home, away]
				);
				page_name_list.push("history_public_league_standings_"+league_name);
			}
		}
		var query = "INSERT INTO history_public_league_standings VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));"
		return [query,queryArray,page_name_list];
	}
	
	function collect_for_check_public_league_standings(page)
	{
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		var team_id_arr = [];
		for (var i = 0; i < trs.length; i++)
		{
			var team_id = getUrlVars($(trs[i]).find("a")[0].href)["team_id"];
			if(undefined==team_id)
			{
				error_notice("team_id == undefined in 306");
				return 0;
			}else team_id_arr.push(team_id);
		}
		var queryArray = [];
		var query = 'SELECT count(*) as c from history_daily where "page_name" in (?) and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour")) group by page_name';
		var tmp="";
		for (var i = 0; i < team_id_arr.length-1; i++)
		{
			tmp = tmp+ '"history_public_team_info_players_'+team_id_arr[i]+'", ';
		}
		tmp = tmp+'"history_public_team_info_players_'+team_id_arr[team_id_arr.length-1]+'"';
		queryArray.push([tmp]);
		return [query,queryArray];
	}
	
	function executeSql_collected(tx,e, query, queryArray, page_name_list)
	{
		for (var i = 0; i < queryArray.length; i++) 
		{
		if ((queryArray[i]) != undefined)
			(function (query, queryArray,page_name) 
			{
				tx.executeSql(query, queryArray, function (tx, result)
				{
					insert_history_daily(tx,page_name);
				}, function (tx,e) {
					error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
					});
			})(query,queryArray[i],page_name_list[i])
		}
	}
	
	function transaction_collected(list)
	{
		db.transaction(function (tx,e)
		{
			for(var i=0; i<list.length; i++)
			{
				(function (tx,e,query, queryArray, page_name_list) 
				{
					//console.log("deferred.done:");
					//console.log(query);
					//console.log(queryArray);
					//console.log(page_name_list);
					dbManagerParser.executeSql_collected(tx,e,query, queryArray, page_name_list);
				})(tx,e,list[i][0],list[i][1],list[i][2])
			}
		}, null, function() 
		{
			notice("Процесс успешно завершён",10000);
		});
	}
	
	//то же что и download только для списка
	function download_list(page_name_list, url_list, done_string)
	{
		var deferred = jQuery.Deferred();
		try
		{
			if (window.openDatabase)
			{
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db)
				{
					db.transaction(function (tx,e) 
					{
						var deferred_results=[];
						for (var i = 0; i < page_name_list.length; i++)
						{
							(function (index, tx,e) 
							{
								tx.executeSql('SELECT count(*) as c,page_name from history_daily where page_name=? and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour"));', [page_name_list[index]], function (tx, results)
								{
									var result = false;
									if (results.rows && results.rows.length)
									{
										if(results.rows.item(0).c<1)
											result = true;
										if(!result)
											notice("Повтор "+page_name_list[index]+",\n сегодня эти данные уже собирались");
										if(result)
										{
											(function (url, page_name,index,target) 
											{
												$.ajax(
												{
													type: "GET",
													url: url,
													success:function (response)
													{
														deferred_results.push(success_download(page_name,$(response).find("div.clearfix").html()));
														if(target==index)
														{
															notice(done_string,10000);
															deferred.resolve(deferred_results);
														}
													}
												});
											})(url_list[index],page_name_list[index],index,page_name_list.length-1)
										}
									}
								}, function (tx,e) {error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);});
							})(i,tx,e)
						}
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
		return deferred
    }
	
	//процедура определяющая в какую процедуру передать данные после успешной загрузки
	function success_download(page_name,page)
	{
		//notice("success_download");
		//if("history_manager_training_form1"==page_name)
		//	insert_history_manager_training_form1(page);
		//if("history_manager_team_players"==page_name)
		//	insert_history_manager_team_players(page);
		//if("history_manager_team_contracts"==page_name)
		//	insert_history_manager_team_contracts(page);
		if("history_public_team_info_players"==page_name.substr(0,page_name.lastIndexOf("_")))
		{
			var ret = parse_public_team_info_players(page);
			return download_list(ret[0],ret[1],"Команда "+page_name.substr(page_name.lastIndexOf("_")+1,10)+" обработана");
		}else
			if("history_public_league_standings"==page_name.substr(0,page_name.lastIndexOf("_")))
			{
				var ret = parse_public_league_standings(page);
				return download_list(ret[0],ret[1],"Лига "+page_name.substr(page_name.lastIndexOf("_")+1,10)+" обработана");
				
				//var league_name  = page_name.split("_")[page_name.split("_").length-1];
				//insert_history_public_league_standings(page);
			}else
				if("history_public_player_info"==page_name.substr(0,page_name.lastIndexOf("_")))
				{
					var player_id  = page_name.split("_")[page_name.split("_").length-1];
					return collect_public_player_info(page);
/* 					db.transaction(function (tx,e)
					{
						executeSql_collected(tx,e,ret[0],ret[1],ret[2]);
					}, null, function() {
						notice("Процесс успешно завершён");
					}); */
					//insert_history_public_player_info(page,player_id);
				}
    }
	
	//процедура вставки метки
	function insert_history_daily(tx,pageName)
	{
		if (pageName != undefined)
			tx.executeSql("INSERT INTO history_daily VALUES(NULL, ?, datetime('now'));", [pageName], function (tx, result)
			{
				notice("Информация "+pageName+" сохранена",10000);
			}, function (tx,e) {
				error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
				});
    }
	
	function deferrers_public_league_standings(deferrers)
	{
		$.when.apply(null,deferrers).then
		(
			function()
			{
				var list = [];
				for(var i=0; i<arguments.length; i++)
				{
					for(var j=0; j<arguments[i].length; j++)
					{
						list.push(arguments[i][j]);
					}
				}
				transaction_collected(list);
			},
			function()
			{
				for(var i=0; i<arguments.length; i++)
				{
					console.log("deferred.fail:");
					console.log(arguments[i]);
				}
			}
		)
	}
	
    return {
        //создание таблиц если не сущуствуют
        create_tables: create_tables,
		collect_public_player_info:collect_public_player_info,
		parse_public_team_info_players:parse_public_team_info_players,
		parse_public_league_standings:parse_public_league_standings,
		collect_public_league_standings:collect_public_league_standings,
		collect_for_check_public_league_standings:collect_for_check_public_league_standings,
		executeSql_collected:executeSql_collected,
		download_list:download_list,
		success_download:success_download,
		transaction_collected:transaction_collected,
		deferrers_public_league_standings:deferrers_public_league_standings
    };
})("haarp.db", "0.2");