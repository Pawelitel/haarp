﻿$(document).ready(function()
{
	//функция разбора url
	function getUrlVars(href)
	{
		var vars = [], hash;
		var hashes = href.slice(href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	//только для страницы с трансфером
	//if(getUrlVars(window.location.href)["p"]=="public_player_info.inc")
	{
		$(document).on("click", "a:contains('Далее')", function () 
		{
			var nextPrevBlock = $("a:contains('Далее')"); // блок с кнопкой Далее
			var href = nextPrevBlock.attr("href"); // ссылка кнопки Далее
			var table = $($("tbody")[1]);
			table.append('<div id="loading-ajax"><img src="http://haarp.redirectme.net/media/images/icon_ajax_loader.gif"/></div>');
			$.ajax(
			{
				url:href,
				success:function (response) 
				{
					var wraps = $(response).find("tr.sr1");//загрузка следующей страницы
					var wraps2 = $(response).find("tr.sr2");//загрузка следующей страницы
					wraps.each(function(){table.append(this.outerHTML);});//добавление строк
					wraps2.each(function(){table.append(this.outerHTML);});//добавление строк
					$("#loading-ajax").each(function(){$(this).remove();});
					//из ссылки "Далее" берёт последнее значение offset(например 25) и добавляет в конец &offset=50
					//по хорошему нужно ещё удалять из ссылки старое значение offset
					$("a:contains('Далее')").attr("href",$("a:contains('Далее')").attr("href")+"&offset="+(parseInt(getUrlVars($("a:contains('Далее')").attr("href"))['offset'])+25));
				}
			});
			return false; // отменяем переход по ссылке
		});
	}
}