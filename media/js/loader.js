﻿//лоадер проверяет тукущий путь и загружает только нужные скрипты

var haarp_id="mpeileipcfncjcnfdceddmklofdfpgen";
//var haarp_id="mpeileipcfncjcnfdceddmklofdfpgen"; //пакованный
//pcangmgodamklebcpgnjlhamccbkkcge // распакованный д
//ocipkfeapenahcfjncponlocokngffam // распакованный р

//функция разбора url
function getUrlVars(href) 
{
	var vars = [],
		hash;
	var hashes = href.slice(href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) 
	{
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	if(vars["p"]==undefined)
	{
		vars = [];
		var pageName=href.slice(href.lastIndexOf('/') + 1);
		vars.push(pageName);
		vars["p"] = pageName;
	}
	return vars;
}

function init ()
{
	//возвращает имя файла из имени и пути js
	function hname(href)
	{
		return href.slice(href.lastIndexOf('/') + 1);
	}
	console.log("инициализация HAARP");

	var href = getUrlVars(window.location.href)["p"];

	//массив соответсвия href и скрипта обрабатывающего эту страницу
	var pages = 
		{
            "index.php?smenu=mpublic": "smenu=mpublic.js",						//дэйлик
            "manager_player_market_sql.php": "manager_player_market_sql.js",	//трансферный рынок
            "manager_team_contracts.php": "manager_team_contracts.js",			//игроки контракты
            "manager_team_players.php": "manager_team_players.js",				//игроки атрибуты
            "manager_training_form1.php": "manager_training_form1.js",			//игроки треньки
            "manager_calendar.php": "manager_calendar.js",						//календарь
			"manager_transfer_list_sql.php":"manager_transfer_list_sql.js",		//прошлые трансферы
			"public_team_info_players.php": "public_team_info_players.js",		//список игроков команды
            "index.php?smenu=mliga":"smenu=mliga.js",							//список команд текущей лиги
			"public_league_standings.php":"public_league_standings.js",			//список команд произвольной лиги
			"manager_player_market_form.php":"manager_player_market_form.js"
        };

	//загрузка jquery, noty, offlineDB и соответсвующего скрипта
	if(undefined!=pages[href])
	{
		console.log("Загрузка скриптов и стилей для "+href);
		yepnope({
			load: ["chrome-extension://"+haarp_id+"/media/js/minimized/jquery-1.7.2.min.js"
				 , "chrome-extension://"+haarp_id+"/media/css/jquery.noty.css"
				 , "chrome-extension://"+haarp_id+"/media/css/noty_theme_twitter.css"
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/jquery.noty.min.js"
				 , "chrome-extension://"+haarp_id+"/media/js/offlineDB.js"
				 , "chrome-extension://"+haarp_id+"/media/js/webDB.js"
				 , "chrome-extension://"+haarp_id+"/media/js/pages/"+pages[href]],
			callback: function (url, result, key) 
			{
				if(url.indexOf("css")>0)
					console.log("Стиль "+hname(url)+' загружен');
				else if(url.indexOf("js")>0)
					console.log("Скрипт "+hname(url)+' загружен');
			}
		});
	}
	else //особые случаи обрабатываются отдельно
	if ("public_player_info.inc" == href) 
	//"public_player_info.inc.htm": "public_player_info.js",					//игрок
	{
		console.log("Определён "+href);
		yepnope({
			load: ["chrome-extension://"+haarp_id+"/media/js/minimized/jquery-1.7.2.min.js"
				 , "chrome-extension://"+haarp_id+"/media/css/jquery.noty.css"
				 , "chrome-extension://"+haarp_id+"/media/css/noty_theme_twitter.css"
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/jquery.noty.min.js"
				 , "chrome-extension://"+haarp_id+"/media/js/offlineDB.js"
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/jquery-ui-1.8.21.custom.min.js"
				 , "chrome-extension://"+haarp_id+"/media/css/jquery-ui-1.8.21.custom.css"
				 , "chrome-extension://"+haarp_id+"/media/js/pages/public_player_info.js"
				 
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/highcharts.min.js"
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/grid.js"
				 , "chrome-extension://"+haarp_id+"/media/css/panel.css"
				 , "chrome-extension://"+haarp_id+"/media/js/disqus.js"
				 , "chrome-extension://"+haarp_id+"/media/js/minimized/yepnope.1.5.4-min.js"
				 ],
			callback: function (url, result, key) {
			  console.log("Скрипт "+hname(url)+' загружен');
			}
		});
	}; //*/
}

//отложенный запуск js
if (/WebKit/i.test(navigator.userAgent)) { // условие для WebKit
    var _timer = setInterval(function() {
	if (/loaded|complete/.test(document.readyState)) {
	    clearInterval(_timer);
	    init(); // вызываем обработчик для onload
	}
    }, 10);
}